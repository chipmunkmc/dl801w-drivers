# dl801w-drivers

This repository contains drivers for the DigiLand DL801W tablet. The drivers were taken from a running Windows installation, as I couldn't find a recovery partition.

# Installation

First of all, run command prompt as administrator, and navigate to the `drivers` folder

For versions of Windows prior to Windows 10, run
```cmd
for /d %i in (*) do pnputil -i -a %i\*.inf
```

For Windows 10 and newer, run
```cmd
for /d %i in (*) do pnputil /add-driver %i\*.inf /install
```

You will be asked to allow for the installation of drivers multiple times, just allow it each time.

This will install all drivers, however, touchscreen firmware will be missing, and as such, touch input won't work. To fix this, simply copy the `SileadTouch.fw` file to `C:\Windows\System32\drivers`.
